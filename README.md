## Sources de construction pour [the beginner's handbook](https://lescahiersdudebutant.fr/index-en.html)

Ce dépôt intègre les sources pour construire :

- [la documentation en ligne](https://dflinux.frama.io/thebeginnershandbook/)
- [la version PDF](https://framagit.org/dflinux/thebeginnershandbook/raw/master/docs/the_beginners_handbook.pdf)
- [la version PDF imprimable](https://framagit.org/dflinux/thebeginnershandbook/raw/master/docs/the_beginners_handbook_print.pdf) (en noir et blanc)
- [la version ePUB](https://framagit.org/dflinux/thebeginnershandbook/raw/master/docs/the_beginners_handbook.epub)
- [la verison HTML-standalone](https://framagit.org/dflinux/thebeginnershandbook/raw/master/docs/the_beginners_handbook.html.tar.gz)
- [le paquet debian](https://framagit.org/dflinux/thebeginnershandbook/tree/master/docs/paquet_debian/)

### Contents

- mkdocs.yml : fichier de configuration export pour mkdocs
- .gitlab-ci.yml : fichier de configuration pour la création du site en ligne via `pages`

- docs/css/
  - cahiers.css : feuille de style pour mkdocs
  - epub.css : feuille de style pour l'export ePUB
  - les_cahiers_du_débutant.png : couverture pour l'ePUB

- docs/img/ : images pour la documentation en 96dpi

- docs/paquet_debian/ : dossier de création pour le paquet debian.

- docs/intro[pub].md : première page pour les version PDF et ePUB
- docs/foo.md : sources de la documentation
- docs/foo.phatch : actionlists pour phatch (optimisation des images)
- docs/pandoc-lcdd.txt : yaml block pour pandoc
- docs/pandoc-tpl.foo : modèles pour l'export pandoc PDF et HTML

- docs/mklcdd : générer toutes les versions (penser à modifier la date de version pour le paquet debian)
- docs/.mklcdd-foo : scripts autonomes pour chaque version

### Depends

- mkdocs
- pandoc
- ps2pdf
- exiftool
- equivs
