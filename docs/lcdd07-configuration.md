# ![config](img/config.png) Tweaking of your environment

Do you feel you took control of your system ? Now we are going to tweak the 
configuration of your workstation.

Basically, you can modify everything you want on the Debian GNU/Linux desktops 
and tailor your environment to make it fit your personal needs and tastes.

To achieve this, the right-click is the *quasi-ultimate* weapon !

## User interface

![skin](img/skin.png)  
The GNU/Linux environments are known for their great flexibility in terms of 
configuration. However, some desktop are more flexible than others, because of 
their main interface.

Grossly speaking, they all work the same way: a "Control Center" to handle them 
all in the same place, and the right-click for the settings of individual elements.

Most of the functionalities have been addressed during the presentation of the 
desktops (chap.4.2). But let's return to the two Debian main desktops: Gnome and KDE.

### The Gnome-Shell interface

**Gnome is the default desktop** for the Debian installations. This desktop features 
an "all-in-one" interface which makes it a fluid and intuitive environment.

Gnome-Shell ([https://wiki.debian.org/GnomeShell](https://wiki.debian.org/GnomeShell)) 
provides a "uniform" interface: this is what brings this great fluidity. This means 
also that you will not be able to modify everything you want, like on the other 
desktops built with a modular design. But instead of talking about the few things 
you cannot make, let's talk about all the settings available to you.

Let's take the direction Gnome Activity menu > "Tweak Tool".

![Gnome-Shell config launcher](img/gnomeshell1.png)

The opening window includes all the elements of your Gnome-Shell desktop. The 
changes are applied and visible immediately.

![Gnome-Shell: configuring the interface](img/gnomeshell2.png)

After few "clicks" on the various categories, you will easily understand how to 
install, in the top bar, the Application menu, the Places menu or the weather forecast.

![Gnome-Shell: Gnome desktop configured with extensions](img/deb9-gnome-shell-ext-shot.png)

#### Installing Gnome-Shell extensions

Gnome-Shell is enhanced with various extensions available in Debian repositories. 
To install them, run the software manager, category "Add-ons"... simple isn't it?

![launching "Software"](img/deb9-gnome-shell-ext-install-1.png)

![Software manager: select "Add-ons" category](img/deb9-gnome-shell-ext-install-2.png)

You still have to make your choice in the list: a left click on an extensions and 
"Software" offers installation. You will be asked for confirmation.

![List of Gnome-Shell extensions available](img/deb9-gnome-shell-ext-install-3.png)  

![Selecting an extension for installation](img/deb9-gnome-shell-ext-install-4.png)  

![Installation confirmation](img/deb9-gnome-shell-ext-install-5.png)  

Your extension is immediately accessible, here with the extension "Screencast" 
which allows to record a video of your working session:

![l'extension est disponible](img/deb9-gnome-shell-ext-install-6.png)

#### Gnome-Shell keyboard shortcuts

In order to be more efficient, and even if Debian is "mouse-click-oriented", we 
suggest you use the keyboard shortcuts: pressing the "Windows" key, for example, 
switch between the Activities overview and desktop. The overview displays the 
open activities, the dock and the virtual workspaces.

| Shortcuts | Actions |
|------------|---------|
|Alt + F1 ou Win|Switch between the Activities overview and desktop|
|Ctrl + Alt + Tab|Give keyboard focus to the top bar|
|Alt + F2|Pop up command window|
|Alt + Tab|Quickly switch between windows|
|Alt + Maj + Tab|Idem, but in reverse order|
|Alt + ²|Switch between windows from the same group|
|Ctrl + Alt + ↑/↓|Switch between workspaces|
|Ctrl + Alt + Maj + ↑/↓|Move the current window to a different workspace|

#### Gnome Desktop Settings

"Gnome Settings" is available from the Parameter launcher and gives you access 
to all the settings of your environment.

![Gnome-Shell config launcher](img/deb9-gnome-shell-config-launch.png)

![Gnome-Shell: All Settings Panel](img/gnomeshell_cfg2.png)

#### The Gnome Classic interface

If you want to use a more "classical" interface, you can take this option during 
your session sign in: click on the little gearwheel to select the "Gnome Classic" 
option before pressing the "Sign In" button:

![Gnome classic: Environment selection during Sign In](img/deb9-gnome-classic-gdm.png)

![connexion](img/deb9-signin.png)

Your session then opens with a more conventional version of Gnome, but you keep 
your main settings and your tools.

The "Classical" interface is lighter, but offers less visual effects (usefull 
or useless, depending on the view point)

![Gnome Classic desktop on Debian 9 Stretch](img/deb9-gnome-classique.png)

In order to come back to the default Gnome-Shell interface, select the "Gnome" 
option of the gearwheel menu, next time you Sign In.

### The KDE Plasma interface

**KDE** is one historical desktop for the GNU/Linux environments, and did always 
put the emphasis on its extreme customization. The transition to the Plasma 
([https://www.kde.org/plasma-desktop](https://www.kde.org/plasma-desktop)) 
rendering engine did not change  a bit on that point: everything on KDE is modular, 
movable, configurable, and accessible from a very detailed **System Setting Center**.

![KDE on Debian](img/deb9-kde-desktop.png)

#### The System Settings panel

This is where you are going to tweak all your computer and Debian system settings.

Network, audio, video, window appearances, default language, and also the way you 
sign in, and a lot more ... Virtually everything is configurable from this panel. 
Even some desktop special effects integrated in KDE, in Compiz style, can be 
managed from here.

![Debian System Settings on KDE Plasma](img/deb9-kde-settings.png)

Each entry is detailed and you will discover as the configuration possibilities 
of the KDE-Plasma environment.  
If you wish a more "out-of-the-box" like environment, and if your computer 
configuration allows it, you should prefer Gnome or Cinnamon which offer less 
detailed (and less complex) interfaces.

#### KDE and desktop widgets

Plasma allows you to add graphic components to your desktop, sort of small 
'widgets' that display virtually anything on your desktop. From the launcher at 
the top left corner or right-click on the desktop, select "Add Widgets" and 
then drag and drop the desired components to your desktop.

![KDE widgets](img/deb9-kde-widgets.png)

To configure the widgets, left-click on the relevant component to bring up the 
configuration menu. You can then adjust the component, move it or resize it to 
your liking.

#### KDE Activities

The Activity manager let you organize your tasks and keep an eye on them. To 
access it, left-click on the top-left corner menu or right-click on the desktop:

![access to activities](img/deb9-kde-activities1.png)

The activity banner will then appear at the left of the screen and display the 
current and pending activities. Each activity can be configured to separate 
your tasks and associate certain settings (for example, deleting the browsing history).

![La gestion des activités sur KDE](img/deb9-kde-activities2.png)

## System Preferences

![prefs](img/preferences.png)  
The configuration of your system directly impact you user experience. Instead of 
writing a complete chapter about the different ethical and technical concepts 
which led to the Debian software organization, we are going to examine their 
consequences on your daily usage.

In the rest of this chapter, you might be asked to activate the "contrib" and/or 
"non-free" sections of the Debian repositories,  in order to access some 
"less-free" software. In that case, follow the method documented in the chapter 8.1.3.

### Root-User / su-sudo

During the installation, Debian ask you to enter the password for the "root" 
administrator account. This account is common to all the GNU/Linux systems and 
allows to execute administrative tasks on the system.

However, this account is not mandatory. During the installation process, you can 
pass over the the "root" account configuration (leave empty the root password 
fields), and doing so, avoid its creation.

In this case, this is the *first user* registered during the installation who 
will assume the "root" role.

> OK... and then?

Then, this changes the way you launch commands to administrate the system: in 
this manual, every time we ask you to execute a command in "root" mode, we use 
the "**su**" command. But if the "root" account was not created, you must use 
instead the "**sudo**" command, which switch you and "root", and give **your 
own password**.

**In a nutshell** to launch an administrative command:  
**If the "root" account was created**, use "su" + administrator password + command launch:

    su
    > asking administrative password
    apt update && apt upgrade
    > command execution

**If the "root" account was *NOT* created**, use  "sudo + command" + your user password:

    sudo apt update && sudo apt upgrade
    > asking first user password
    > command execution

### Read a commercial DVD

The DVDs sold commercially are "copy protected" and usually are not readable by 
default on free systems.

The Entertaining Companies consider that we do not have the right, on a free system, 
to make a private copy of the DVDs we bought ... no comment.

To remedy that, if you really need this functionality, you must install the 
**libdvdcss2** package. This package is not present by default in the Debian 
repositories, but you can find it in the repositories of Videolan (the site of 
VLC, the well known multimedia player).

**The principle** consists in adding the Videolan repositories to your system, 
adding the signature keys to secure the communication with these repositories 
and finally installing libdvdcss2.

**In practice: 3 commands and that's it!**

**1** Open a terminal in administrator mode (chap.3.8.3) with **su**, then 
launch this command to add the Videolan repositories in a separate file:

    echo "deb http://download.videolan.org/pub/debian/stable/ /" > /etc/apt/sources.list.d/videolan.list

**2** Add the repositories signature key:

    wget -O - http://download.videolan.org/pub/debian/videolan-apt.asc | apt-key add -

**3** Reload the repositories and install libdvdcss2:

    apt update && apt install libdvdcss2

You can realize these modifications in graphical mode only, by using the 
Synaptic package manager, but it takes a lot of time to click with the mouse 
all over the places ...

Your Debian GNU/Linux system is now able to read the "protected" commercial DVDs, 
and to make private copies of them.

### Using FlashPlayer

**Adobe Flash Player** is a **non-free** platform used to add animation, video, 
and interactivity to Web pages. Flash is frequently used for advertisements and games.

A free reader exist: Gnash 
([https://packages.debian.org/stretch/gnash](https://packages.debian.org/stretch/gnash)). 
It is an alternative to the Adobe reader but does not allow to have access to all 
the Flash functionalities.

Be aware that the current trend for site developers is to **move away from flash** 
in favor of HTML5 
([https://fr.wikipedia.org/wiki/HTML5](https://fr.wikipedia.org/wiki/HTML5)), 
thus it is more and more easy to survive on the Internet without the proprietary 
flash player.

This manual being intended for beginners, we are not going to ask you to remove 
all the bookmarks pointing to sites still using the flash reader (although that 
could help making a faster transition), so we will detail below how to install 
the flash player from Adobe website

- Start by visiting the official download page 
([https://get.adobe.com](https://get.adobe.com)) which should detect your 
architecture and your browser. Select the "linux tar gz" archive in the menu:

![flash player download page](img/adobeflash-en.png)

- Once the archive is downloaded, right-click on it the "extract here".
- Open the "flash_player-npapi_linux_your-architecture" folder, you'll find 
the "libflashplayer.so" file.
- Copy this file from your file browser: right-click on libflashplayer.so > "copy"
- Navigate to the folder ~/.mozilla by displaying the hidden folders. (menu view > 
show hidden files).
- Create the folder "plugins" in your ~/.mozilla then right-click in this folder > 
"paste" the libflashplayer.so

To update your flash player, retrieve the latest version in the same way, and 
overwrite the old file with the new one, simply.

### Installing a graphic card driver

The Debian GNU/Linux system includes free drivers and some firmware in order to 
operate the integrated graphical functions (chipsets) of the motherboard or the 
external graphical card.

![noob](img/noob-modarp-circle-icon.png)  
**... I need a driver to manage the card of my mother ??**  
Your computer is designed around its "motherboard", a plastic board on which the 
various elements of the processor (the central compute unit) are fixed, as well 
as the chipsets, the graphical component being part of them (the graphical 
display computer, to make a long story short).  
In order to send the right instructions to these elements the GNU/Linux kernel 
(the program managing the 
interactions of the  hardware with the system) uses pieces of code, called drivers 
(and sometimes "firmware" to be loaded on smart devices during the initialization 
phase). The Linux kernel includes by default a set of free drivers. These drivers 
are usually sufficient to manage the screen display. But it may happen that 
proprietary drivers are needed to optimize the screen resolution.

You can try to install these proprietary drivers if, during a live session or once 
your system is installed, you find out that:

- resolution is wrong, with stretched and distorted images,
- the screen remains black after start-up, even if the system was installed 
without any errors.
- the colors are badly handled
- the system is heating too much when watching videos
- returning from the "suspend" mode is somewhat erratic ...

Please note that installing proprietary drivers is a non-free alternative, and as 
such, not followed by the Debian developers. If your computer is very recent, don't 
hesitate to ask the support of the community before tinkering with your system (chap.1.2).

#### Identifying your hardware

The first thing to do is to identify your graphic device (or graphic card). 
Open a terminal as simple user, and enter the following command:

    lspci | egrep "3D|Display|VGA"

which will display this kind of result for a nVidia controller:

    01:00.0 VGA compatible controller: NVIDIA Corporation G96GLM [Quadro FX 770M] (rev a1)

or this kind of result for an ATI controller:

    01:00.0 VGA compatible controller [0300]: Advanced Micro Devices, Inc. [AMD/ATI] Barts PRO [Radeon HD 6850] [1002:6739]

> but what doest that mean?

    lspci

lists the devices connected to the PCI bus and

    egrep "3D|Display|VGA"

selects only the information we are interested in, rather than displaying 
everything. Here we are looking only at graphical components.  
As you can see the identification is rather easy ... we got the manufacturer and 
the model on the same line. Should you have the slightest doubt concerning the 
installation of additional drivers, don't hesitate to ask the Free Community for 
help (chap.1.2).

#### Installing the generic firmware

In all cases, and before the installation of any proprietary driver, you should 
add the *non-free firmware* delivered by Debian. To add the Debian non-free 
firmware to your system, start with modifying your repositories (chap.8.1.3) 
by adding the *contrib* and *non-free* sections to your sources.

Open a terminal in administrative mode (voir chap.3.8.3) with the "**su**" 
command (the administrator password is requested), then launch the command:

    apt edit-sources

Modify the sources.list file by adding the contrib and non-free section, like this:

    ###### Debian Main Repos
    deb http://deb.debian.org/debian/ stretch main contrib non-free
    
    ###### Debian Update Repos
    deb http://security.debian.org/ stretch/updates main contrib non-free
    deb http://deb.debian.org/debian/ stretch-updates main contrib non-free

Save your modifications with [Ctrl]+x in the Nano text editor, then "Y" for "Yes".  
Reload the repository informatin, and install the non-free firmware:

    apt update && apt install firmware-linux firmware-linux-nonfree

Restart your machine to test again your screen  display before installing the 
proprietary drivers.

If no real improvement is noticed, continue with the following sections ... 
The *sources.list* file is already set for the next steps.

#### ATI/AMD card configuration

**ATI driver**

This is the "generic" driver to install if your card is not listed in the specific 
sections (Radeon, Catalyst).

To install it from an administrator terminal (chap.3.8.3):

    apt install libgl1-mesa-dri xserver-xorg-video-ati

Restart your computer to load the microcode (firmware) of the graphic peripheral.

**Radeon driver**

the Radeon driver of Debian 9 "Stretch", supports the graphic processors from R100 
to Hawaii (Radeon 7000 - Radeon R9 290). See the Radeon package page 
[https://packages.debian.org/stretch/xserver-xorg-video-radeon](https://packages.debian.org/stretch/xserver-xorg-video-radeon) 
to verify your card.

To install it from an administrator terminal (chap.3.8.3):

    apt install libgl1-mesa-dri xserver-xorg-video-radeon

Restart your computer to load the microcode (firmware) of the graphic peripheral.

For non-supported cards here and for more information, visit the official Debian 
documentation: 
[https://wiki.debian.org/ATIProprietary](https://wiki.debian.org/ATIProprietary).

#### nVidia card configuration

Debian uses a software tool to detect and indicate the driver to be installed: *nvidia-detect*.

To install and use it, open a terminal in administrator mode (chap.3.8.3), then launch:

    apt update && apt install nvidia-detect

To launch the detection, simply launch the command:

    nvidia-detect

which will return this kind of result:

    Detected NVIDIA GPUs:
    01:00.0 VGA compatible controller [0300]: NVIDIA Corporation G96GLM [Quadro FX 770M] [10de:065c] (rev a1)
    Your card is supported by the default drivers and legacy driver series 304.
    It is recommended to install the
        nvidia-driver
    package.

In this example, the card is natively supported by the default driver and by the 
"legacy series 304" driver; Depending on your machine, you can install two types 
of drivers: the 340.xx series (for the GeForce 8x and more) and the 304.xx series 
(for the GeForce 6x and 7x). Older models are supported natively by the free driver.

For a **complete list of the supported cards**, visit the dedicated documentation: 
340.xx series page 
([http://us.download.nvidia.com/XFree86/Linux-x86_64/340.65/README/supportedchips.html](http://us.download.nvidia.com/XFree86/Linux-x86_64/340.65/README/supportedchips.html)) - 
304.xx series page 
([http://us.download.nvidia.com/XFree86/Linux-x86/304.125/README/supportedchips.html](http://us.download.nvidia.com/XFree86/Linux-x86/304.125/README/supportedchips.html))

For details on the installation procedure and supported cards, refer to the official 
Debian documentation page: 
[https://wiki.debian.org/NvidiaGraphicsDrivers](https://wiki.debian.org/NvidiaGraphicsDrivers)

#### Perte de session graphique

If, following a driver installation, you are facing a black screen, you need to 
return to the previous configuration.

**Boot in "recovery mode"**: from the Grub loader menu, select the "Advanced options" 
entry and then the "recovery mode" entry.

![GRUB: advanced options](img/deb9-recovery-1.png)

![GRUB: recovery mode](img/deb9-recovery-2.png)

The system launches a console and invite you to continue the startup sequence 
(by using the [Ctrl]+d short-cut to quit the console) or enter the administrator 
password, and this is what you do:

![recovery mode enabled](img/recovery3.png)

**Removal of the xorg.conf configuration file**: during the installation of the 
proprietary driver, you created a X configuration file located in /etc/X11/xorg.conf 
and/or /etc/X11/xorg.conf.d/xxx.conf. It must be removed. Always from the console 
and depending on the file created, remove it with the command: "**rm**":

    rm /etc/X11/xorg.conf.d/20-nvidia.conf

Here this is the file corresponding to **nVidia** which was removed, 
**!! to be adapted to your situation !!**.

**Uninstallation of the proprietary drivers**: The same way you were able to install 
a driver, you can uninstall it, here an example with the nVidia driver, 
**!! to be adapted to your situation !!**:

    apt remove nvidia-kernel-dkms nvidia-driver

**Restart your computer** with a simple

    systemctl reboot

## Adding a new user

![users](img/users.png)  
If you are not the only user of your computer, you can create new user accounts 
in order to **preserve your data and preferences**. By adding a new user, a new 
folder will be created in the system. This folder, named by the pseudo of the 
new user, will receive the default parameters delivered during the system 
installation.  
It will be readable (you can consult the data of the other user) but not 
writable (you cannot create or modify its data).

### Adding a new user the graphical way

Debian includes a simple graphical tool to execute this task, but you can also 
use the terminal and the "adduser" command (described in the next section).

From a menu or the application list, "System" category, select "Users and Groups":

![user config launcher](img/usegroup.png)

The window which opens, displays the detailed information of your user account. 
You can then modify the parameters of your account, or create a new account. 
But you need first to "unlock" the application by clicking on the *lock* 
button at the top, and enter the administrator password.

**To add a new account**, click on "Add" (or the "**+**" button) at the 
bottom of the left panel:

![user configuration](img/useraddgui-1.png)

You must enter the full name of this new user, as well as its username (the 
pseudo or login name used when connecting to a new session). On the Gnome 
system, you can let the user to define its password during the first 
connection, or enter it immediately:

![adding new user](img/useraddgui-2.png)

The user account is created ... and here you go:

![new user password](img/useraddgui-3.png)

Advanced settings allow you to define the rights and permissions for each user.

### Adding a new user using the terminal

Open a terminal in administrator mode (voir chap.3.8.3) enter the command which 
creates the new "*username*" account. Note that you should enter here the user 
ID (or pseudo), not the full name which will be asked later on:

    adduser username

![useradd 1](img/adduser-1.png)

The user account creation process is started and you must enter the password 
twice (without any echo, its normal).

Once the account is created, we can enter additional information: full name, 
room number, work phone, home phone, and any comments in the "Other" field.  
Once done, a confirmation is requested. If these information are correct you 
can type [Enter] or [Y] to finish the account creation process:

![user added](img/adduser-2.png)

This new user will be able to login with its username and password at computer 
startup. It will be able to store and manage its data and configure its 
environment without any risks for your own data and preferences.

