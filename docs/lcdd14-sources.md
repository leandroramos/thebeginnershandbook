# ![sources](img/src.png) Sources & licenses

Sources of this manual are freely available in its GIT repository: 
[https://git.framasoft.org/dflinux/thebeginnershandbook](https://git.framasoft.org/dflinux/thebeginnershandbook)

## Software used

- Sources edition: Geany [https://www.geany.org/](https://www.geany.org/) & 
VIM [http://www.vim.org/](http://www.vim.org/)
- Screenshots: scrot 
[https://en.wikipedia.org/wiki/Scrot](https://en.wikipedia.org/wiki/Scrot)
- Pictures edition: Gthumb 
[https://wikipedia.org/wiki/GThumb](https://wikipedia.org/wiki/GThumb), 
the Gimp [https://www.gimp.org](https://www.gimp.org), 
Inkscape [https://inkscape.org](https://inkscape.org) & Phatch 
[http://photobatch.wikidot.com](http://photobatch.wikidot.com)
- HTML public export: MkDocs [http://www.mkdocs.org/](http://www.mkdocs.org/)
- External exports in PDF, HTML & EPUB : Pandoc [http://pandoc.org/](http://pandoc.org/)
- Debian package export: equivs 
[https://debian-handbook.info/browse/stable/sect.building-first-package.html](https://debian-handbook.info/browse/stable/sect.building-first-package.html)
- PDF optimization: ps2pdf [https://www.ps2pdf.com/](https://www.ps2pdf.com/) & 
Exiftool [https://en.wikipedia.org/wiki/ExifTool](https://en.wikipedia.org/wiki/ExifTool)

## References

**The various sources that helped me in the development of this manual:**

- Debian Administrator HandBook: 
[https://www.debian.org/doc/manuals/debian-handbook/index.html](https://www.debian.org/doc/manuals/debian-handbook/index.html)
- Debian main site: [https://www.debian.org/](https://www.debian.org/)
- The Jessie beginner's handbook: 
[https://lescahiersdudebutant.fr/jessie/index.html](https://lescahiersdudebutant.fr/jessie/index.html)
- Debian-Facile documentation (fr): [https://debian-facile.org/wiki](https://debian-facile.org/wiki)

## Coordination & License

The beginner's handbook scribbled by **3hg team** ![3hg.fr](img/bouton3hg.png) 
[https://3hg.fr](https://3hg.fr) under **free license WTFPLv2** ![wtfpl](img/wtfpl.png) 
([http://www.wtfpl.net](http://www.wtfpl.net)) *unless otherwise stated*.

This manual integrates logos (Debian, Firefox, etc.) under copyright 
*(each one its own, otherwise it's not funny)* as well as some images and texts 
under license CC-BY-SA ![cc by sa](img/ccbysa.png) 
([https://creativecommons.org/licenses/by-sa/3.0/](https://creativecommons.org/licenses/by-sa/3.0/)) 
(specified and credited under images and texts Concerned)  
The icons used come from themes Gnome:  
[https://commons.wikimedia.org/wiki/GNOME_Desktop_icons](https://commons.wikimedia.org/wiki/GNOME_Desktop_icons)  
& Tango: [https://commons.wikimedia.org/wiki/Tango_icons](https://commons.wikimedia.org/wiki/Tango_icons)

**Debian** : Copyright © 1997-2017 SPI ([http://www.spi-inc.org/](http://www.spi-inc.org/)) 
and others; See license terms ([https://www.debian.org/license](https://www.debian.org/license))  
![gplv3](img/gplv3.png)  

Debian is a registered trademark 
([https://www.debian.org/trademark](https://www.debian.org/trademark)) 
of Software in the Public Interest, Inc.

## Speakers *aka* thx a lot

First of all, a great **Thanks** to the Framasoft team ![Framasoft](img/button_80x15_framasoft.png) 
([https://framasoft.org](https://framasoft.org)) that hosts the sources of this project on framaGIT 
and the [online public version](https://dflinux.frama.io/thebeginnershandbook/).

Thanks a lot to people who participated to the first "Jessie" release of the beginners handbook: 
bendia, nIQnutn, Atapaz, Severian, deuchdeb, martinux_qc, mercredi, nazmi, chalu, bruno-legrand, 
Thuban, Starsheep, arpinux, smolski, Trefix, desmoric, nono47, yanatoum, PengouinPdt, èfpé, 
fiche, BibiSky51, titiasam, Firepowi, dcpc007, rhyzome, Péhä, Caribou22.

For this Stretch release, thanks to BibiSky51 for its rereading and advices, to kyodev for gitpages 
initiation, to 3hg for work and support, to saby43 for the english translation and to my wife <3

**Special thx 2 Péhä for his drawings (under CC-BY-SA) and his free spirit** 
[https://lesptitsdessinsdepeha.wordpress.com](https://lesptitsdessinsdepeha.wordpress.com).

![by Péhä CC-BY-SA](img/walltlpd-bypeha-ccbysa.png)

