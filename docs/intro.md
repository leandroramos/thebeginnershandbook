
---

**-- About this manual --**

**"The beginner's handbook"** is a simplified manual to install and take-over the 
Debian system.

You will find in the following pages **the answers to your first questions** 
concerning the **Debian GNU/Linux** system ![](img/gnulinux-mini.png), its history, 
how to obtain it, to install it, to take-over it, to configure and administrate it.

You will be able to **go further** and obtain information concerning the privacy 
protection, the backing up of your data, and the various actors of the Free Software world.

Usually, the manuals begin by teaching you the theoretical basis and the usage of the 
terminal. This manual takes the very side of the "graphical environment": it is 
designed to let you start quickly with Debian, screen powered on, fingers on the 
keyboard and the mouse nearby ![](img/big_smile.png).

---

**-- The mission of this manual is not to be comprehensive. --**

A lot of external links are available in this manual. Don't hesitate to click on 
them in order to read more detailed information.

For a more detailed documentation, please visit the official Debian Wiki: 
[https://wiki.debian.org/FrontPage](https://wiki.debian.org/FrontPage)

If you need a complete Debian manual, please read the **Debian Administrator Handbook** 
from Raphaël Hertzog and Roland Mas 
[https://debian-handbook.info/browse/stable/](https://debian-handbook.info/browse/stable/).

---

**-- How to use this manual?--**

This PDF version includes a detailed summary and a table of figures at the end of the guide.  
Note: This manual includes some commands or code blocks that are sometimes longer than 
the width of the page. In this case, a backslash `\` is added and the rest of the 
command or code is carried over to the next line.

---

![cahiers minibanner](img/minibanner-cahiers.png) The Debian beginner's handbook: 
[https://lescahiersdudebutant.fr/index-en.html](https://lescahiersdudebutant.fr/index-en.html)

