# ![initiation](img/initiation.png) A simplified introduction to computer systems

**We begin here a scary page...**

**Let's start with a simple statement: Software developers and end-users do not 
speak the same language...**

![dev vs users](img/dev_vs_user.jpg)

**But then ... how are we going to make it?**

> Easy: we read the beginner's handbook! ![](img/love.gif)

Within a short period of time, computers became an essential tool of the modern life. 
One issue is that information and education don't follow the technical progress at 
the same pace, nor the needs of the users.

Therefore, it is difficult for a "new user" to discover the full capabilities of its 
computer. Moreover we use generally only few applications.

We will try to simplify this universe as far as possible, in order for you to make 
the most of your computer and to enjoy the capabilities of Debian.

**How does a computer work ?**

One launches applications, clicks on icons, types text in... One needs a computer, 
a screen, a keyboard, a mouse.

In this handbook, we will explain to you the basic manipulations in order to use 
your mouse ![mouse](img/mouse.png) and your keyboard ![clavier](img/keyboard.png).

**What use for a computer?**

It seems difficult to summarize in few sentences the whole scope of information 
technology. However its usage is somewhat clear : 

- **watch a movie**:  whether it is for your summer vacation footage, or a DVD, or 
a video file downloaded from Internet, Debian offers you several multimedia players. 
An example in this manual with VLC (chap.6.7)
- **listen to music**: enjoy your audio CD, your direct radio streaming, or your 
digital music library with  Rhythmbox (chap.6.8) and no trouble at all.
- **search for something on Internet**: browse the Net, visit pages, contribute to 
Internet, by using several  Debian web applications let's take Firefox as an example (chap.6.6).
- **read or write electronic mails**: communicate with your family, your contacts, 
using either your email client or your web browser (chap.6.4)
- **work on formatted documents or presentations**: Debian includes several 
applications, but the LibreOffice suite (chap.6.9) will let you perform all kinds 
of office work using compatible formats.
- **walk through your family pictures**: your memories in one click, simply using 
the image viewers integrated on all the Debian desktop.
- **print documents or images**: Debian uses the CUPS printing server, and its 
common configuration tool (chap.6.2.2), but you can also use an integrated utility 
for a printer simplified configuration (chap.6.2.1)

And this is exactly the knowhow you are going to learn with **the beginner's 
handbook** ![](img/cool.png).

## Test your level of computer skills

Keep in mind that this manual is not cast in stone... Our advices are only suggestions 
regarding your computer knowledge ...

First of all, you are free [](img/big_smile.png) !

Importantly,  be as honest as possible: we are all beginners in one domain or another 
(personally, I am unable to change the injectors in the carburettor of my car) and it 
is not a fault. The issue is raising only because computers are taking a large space 
in our lives, and penalize the newbies. But we are here to change all that!

The goal of this manual is not to transform you in *GNU/Linux sysadmin* (short for 
system administrator), but simply give you the tools to **utilize your computer as you want** !

**Outright beginner?**

You never or rarely used a keyboard? You still wonder why you must "open a window" and 
what is this "drag and drop" concept? Please continue reading this manual and follow 
its guidance. You will learn how to:

- use your mouse and keyboard: the tools to directly interface with the machine,
- recognize the basic elements of your desktop environment: menus, panels, windows, 
virtual desktop ...
- and then discover Debian and its functionalities.

**Novice user?**

You are a Windows® user and/or you have a little experience with GNU/Linux, but you 
never installed it: it is the right time to choose your Debian (chap.4) and 
discover the main interface of your future system.

**Basic user?**

You have already used a Debian derivative and/or another free distribution, and 
you know exactly what you  need. Let's jump directly to the serious stuff with 
the actual installation (chap.5).

## The mouse

The mouse is the **physical interface** which let you move the **pointer** on 
the screen: the mouse moves are synchronized with those of the of the little 
arrow (the pointer) ![pointeur](img/pointeur_default.png) on your desktop.  
There are different [types of mouse](https://en.wikipedia.org/wiki/Computer_mouse); We 
will take here the example of the classical mouse with two buttons and a scrolling wheel.

### Left-click and double-click

The **left-click** ![clic-gauche](img/clic_gauche.png) (or simple-click) is the most 
common and is used to **point to** (or select) either a folder, or a file or an 
image, which can then be **open** with a **double-click** (done by quickly pressing 
twice the mouse left-button). This left-click is also used to send commands to the 
computer (validating a choice for example) when one presses on the "button" or 
something else sensitive to the click (like the cross closing a window, for example).

### Right-click

The **right-click** ![clic-droit](img/clic_droit.png) is used to open a contextual 
menu (a variable list of options, depending on the software used and the "object" 
pointed at by the mouse) in order to modify a file, a folder, a configuration ...

### Middle-click

The **middle-click** or **scrolling-wheel** ![clic-central](img/clic_central.png) 
is used for scrolling and quick copy. If your mouse has neither a middle button 
nor a scrolling wheel, the "middle-click" can be emulated by pressing on the two 
(left and right) buttons at the same time.

### Actions executed with the mouse

The main action of the mouse is to point an item to open it (in the case of a 
document for example) or to launch it (in the case of a link to an application or 
a menu entry). For that, nothing very complicated, just place the pointer on the 
element and then double-click with the left button of your mouse.

One thing that you **SHOULD NEVER DO** is to click several times on a button if 
you think that nothing happens. It is very possible that an application won't 
start "immediately", it is very depending on your hardware and the application 
being launched. As an example, a web browser takes significantly more time to 
start up, than the file manager.

#### Drag-and-drop

To graphically move or copy your data, it is enough to "drag" them across the 
screen and "drop" them where you want (this is the graphical equivalent of 
the **mv** command).

Example: to move a file you just downloaded into another folder, press the 
left-button the file in question, and while you hold the right-button, you 
move the mouse into the destination folder and then you release the mouse 
button:

![drag and drop: point to the file to be moved](img/glisser_deposer_1.jpg)  

![drag and drop: hold the button while moving the mouse](img/glisser_deposer_2.jpg)  

![drag and drop: move the mouse into the destination folder](img/glisser_deposer_3.jpg)  

![drag and drop: release the button](img/glisser_deposer_4.jpg)  

#### Text selection

Put the cursor at the beginning or at the end of the text segment you want to select, 
then hold the left-button, and move the mouse over the text you want selected. Then 
release the mouse button.

You can also double-click (click twice quickly on the mouse left-button) on the 
first word you want to select and then move the cursor.

![text  selection within the Gedit application](img/selection_texte.jpg) 

If you are fast enough, a triple-click will select the entire line or paragraph.

#### Copy and paste a selection

**With the right-button**: a right-click will display a contextual menu giving 
you the choice among several actions, one of them being the requested copy/paste. 
Put the cursor within the selected segment, right-click and choose the "copy" 
action. Then move the cursor where you want to paste the selected text, 
right-click again and choose "paste".

![Copy and paste a text segment: copy the selection](img/copier_coller_1.jpg)  

![Copy and paste a text segment: cursor on destination, right-click > paste](img/copier_coller_2.jpg)  

![Copy and paste a text segment:  selection pasted](img/copier_coller_3.jpg)  

**With the middle-button**: this is the fastest method. Once the text segment 
is selected, you just need to move the pointer where you want to paste the 
selection and do a middle-click. The copy is immediate.

#### Selection of several items

If you nedd to move, copy er delete several items, you can select them together.

To select a group of contiguous items: press and hold the left-button, move the 
mouse to drag a frame around them and release the button when they are all 
selected. Then you can act on the selection like explain previously (copy/paste 
or contextual menu)

![Selecting several folders under Gnome](img/selection_contigus.jpg)

To select non contiguous items you can:

- either select each item one by one with a combination of the [Ctrl] key on 
the keyboard and the left-button of the mouse: hold the [Ctrl] key down and 
left-click on each element you want to select.
- or select all the elements and then "remove" the undesirable ones using a 
combination of the [Ctrl] key on the keyboard and the left-button of the 
mouse: hold the [Ctrl] key down and left-click on each element you want to 
remove from the selection.

![Selection of several non-contiguous items in a Nautilus window](img/selection_non_contigus.png)

## The keyboard

The keyboard is **the main physical interface to enter data** in your computer. 
But it is not the just the device which let you enter some words in the Internet 
search bar, or work with a word processor. It includes also some special keys, 
called **modifier keys**, which allow you to execute quick actions by modifying 
the behavior of the "normal" keys. The combinations of some "special" keys with 
other "normal" keys form the **keyboard shortcuts**.

**Default QWERTY keyboard layout**

![Example of layout for an English keyboard (cc-by-sa)](img/clavier_qwerty.png)  

### The modifier keys

The "non-alphanumeric" keys of the keyboard give you access to extended functionalities 
during action or edition phases. From the simple carriage return within a text editor 
with the [Enter] key, to the launch of a Help window with the [F1] key, find hereafter 
some descriptions of these special keys:

- **[ENTER]** The first "special" key, which is not really a modifier key. This is 
the most important key of your keyboard since it let you end a command line, launch 
a search request. Basically this is the key saying "Yes" to the computer. When a 
dialog window opens on the screen, either to confirm a download or delete an application, 
take the time to read the message before pressing [Enter].
- **[Ctrl]** or **[Control]** Located at the bottom of your keyboard, on both sides 
of the space bar, it is the default key used for the shortcuts.
- **[Alt]** or **[Function]** By default this key displays the specific shortcuts of 
an application. Within an open window, pressing the [Alt] key reveals the shortcuts 
to navigate through the menus or trigger some actions. These shortcut keys are 
identified by an underscore.
- **[AltGr]** let you use the hidden characters of the keyboard. More information 
in the dedicated section (chap.2.3.3).
- **[ESC]** or **[Escape]** This key cancels the latest entry of a modifier, or 
closes a dialog box asking the user to make a choice (equivalent to click on the 
"Cancel" button in the dialog box).
- **[Tab]** or **[Tabulation]** Symbolized by two opposite horizontal arrows. It 
allows you to complete a command or navigate through the various fields of a form, 
or menus of a window.
- **[Shift]** or **[Uppercase]** Key symbolized by a wide up-arrow. let you type 
capitalized characters, and sometimes numbers (depending on your keyboard layout), 
some special characters like "@","%", "&" etc.
- **[CapsLock]** or **[Capital Lock]** Key symbolized by a lock or a larger [Shift], 
causes all letters to be generated in capitals until deactivated. Equivalent to a 
[Shift] key down permanently.
- **[F1]**, **[F2]**...**[F12]** Execute various functions... by definition. The [F1] 
key is often used to launch the Help function within applications, [F11] to switch 
to full-screen mode; For example.

### Keyboard shortcuts

*Why bother?* **Because it's much faster!** ![](img/big_smile.png)

Note that the shortcuts are made by pressing down the keys together, at the same time: 
to copy a selection, press and hold the [Ctrl] key, then press the 'c' key. You can 
then release both keys, a clone of your selection is stored in the "clipboard" 
(a special buffer located in the memory of the operating system).  
Hereafter a short summary of the most useful **keyboard shortcuts**:

| shortcut | action |
|----------|--------|
| [Shift] + arrows | make a selection |
| [Ctrl] + 'c' | Copy the current selection (in the "clipboard") |
| [Ctrl] + 'x' | Cut the current selection (and save it in the "clipboard") |
| [Ctrl] + 'v' | Paste the latest copy/cut selection |
| [Ctrl] + 'f' | Find a word or an expression |
| [Ctrl] + '+/-' or Mouse-scrolling-wheel | Zoom in/out of the screen display |
| [Alt] + [F4] | Close the active window |
| [Alt] + [Tab] | Jump from open window to the next one |
| [F1] | Open the Help function of the active application |
| [F11] | Switch to full-screen mode |

![noob](img/noob-modarp-circle-icon.png)  
Note that some functionalities are not only available on text segments (like 
copy/paste), but on files also: if you select several pictures in your 'Pictures' 
folder, make a [Ctrl]+'c' and then a [Ctrl]+'v' on your desktop, your selected 
pictures will be copied there. In the same way, [Ctrl]+mouse-scrolling-wheel will 
zoom in or out the content of your Internet navigator, as well as the content of 
a system file manager window.

### Special characters

Keyboards can't contain as many keys as available characters. In order to write 
the particular characters in English, it is necessary to combine the keys like 
the shortcuts (simultaneous pressing of the keys)

First, a preview of the QWERTY's hidden keys:

![special charaters available on QWERTY keyboard layout (cc-by-sa)](img/clavier_qwerty_xtra.png)  

Each symbol is associated with a key combination. So to write "©", you'll have 
to simutaneous press [AltGr] + 'c'.

![Available characters on 'c' key](img/caracteres_touche.png)  

If you are not comfortable with simultaneous pressure exercises, you can always 
copy and paste special characters from 
[a characters table](http://www.jchr.be/html/caracteres.htm#).

## Online exercises

**The beginner's handbook dedicated page (mouse & keyboard):**  
[https://lescahiersdudebutant.fr/exercices/index-en.html](https://lescahiersdudebutant.fr/exercices/index-en.html)

**mouse exercices:**

- [http://pbclibrary.org/mousing/mousercise.htm](http://pbclibrary.org/mousing/mousercise.htm)
- [http://www.mydigitalliteracy.us/](http://www.mydigitalliteracy.us/)

**keyboard exercices:**

- [http://play.typeracer.com/](http://play.typeracer.com/)
- [https://www.typing.com/student/start](https://www.typing.com/student/start)
- [http://www.sense-lang.org/typing/tutor/keyboarding.php](http://www.sense-lang.org/typing/tutor/keyboarding.php)

## The users

One of the great strengths of the GNU/Linux systems is their user management. 
The separation of rights and responsibilities provides a better security when 
executing system administration tasks or exchanging data on the net. 
Small explanation ...

### $USER

![user](img/icon_user.png)  
Generally YOU are the user, of course. Sometimes one talks about the 
Chair-To-Keyboard interface (abbreviated CTKI), since it's very true that you are 
sitting between the chair and the keyboard, or the mouse. When you work on your 
computer, it does not see you. It feels only the actions made by a user with 
a *login-name* and sometimes a *password*.

Each user is allowed to perform a certain number of actions. Your user can, for 
example, use the keyboard and the mouse, read and write some documents (files), 
but not all of them. We call that *rights*: to execute administrative tasks, 
one must obtain the rights of the **root** administrator (chap.3.8.3).

### ROOT

![root](img/icon_root.png)  
Only one user has all the rights, it is the *administrator*. This special user 
is able to execute some tasks (in particular for the system administration) 
that other *normal* users cannot perform by themselves. But a single mistake 
in an operation made by this *root* user could potentially beak the whole system.

At home, on your desktop computer , you can use your computer both as a *normal user* 
and as an *administrator*. Some well defined actions have to be made in order to 
switch from one role to the other, like entering the root administrator password 
(chap.3.8.3).

### Separate to secure

![chose](img/icon_chose.png)  
This clean distinction, which, by the way, does not always exist under other 
operating systems, strengthen the stability and security of the Debian GNU/Linux 
system, as mentioned at the beginning of this manual. When working as *simple/normal* 
user you cannot make your computer unusable (brick it), and the potential viruses 
cannot infect the whole system.

**More details on rights and permissions in the chapter 3.7.**

