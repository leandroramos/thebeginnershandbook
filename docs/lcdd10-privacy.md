# ![privacy](img/lock.png) Protect your data and your privacy

The news are crystal clear: is being wire-tapped. It is not to alarm or frighten 
you, but you have to realize that the "Internet" is not your private living room, 
and that each picture or text used on the web is potentially recoverable.

## Protect your system

### Physical security

This is obvious, but let's say it but once and for all: if you want to protect 
your data, **don't leave your laptop everywhere ! don't leave your computer in 
self-service at your home !**

Your computer hosts your passwords (bank, administration, work ...) your 
documents (administrative forms, pictures, etc.), your browsing history (the 
sites you visited and when), etc. You may think that these information are 
seemingly harmless, but they allow - for the best - to define your consumer 
profile, or - for the worst - to use your computer as a gateway to crack in 
other persons systems.

If you want to present your distribution, or simply share tour resources, 
we strongly suggest you create another user account (chap.7.3) who will not 
be able to access your data, nor to mess with your system administration. 

![noob](img/noob-modarp-circle-icon.png)  
If your computer is always on the road, we suggest you use the direct 
encryption during the system installation like mentioned in the chapter 5.5.3. 
By this way, even if your computer is lost or stolen, it will be extremely 
difficult to extract the data from your hard disk.

### Updates

The software **updates** bring new functionalities, fix bugs, and, above 
all, **correct potential security flaws**.

This is the big strength of the Libre-Software: **the program sources are 
available**, thus when a flaw is uncovered, it is publicized and fixed immediately.

This practice to be opposed to the proprietary systems, whose flaws are being 
kept secret, and continue to affect the daily life of all their users.

The **security updates** should not be considered as optional ones: you must 
install them as soon as possible.

### Passwords

Each year, tens of thousands of email accounts, Wi-fi access codes, phone 
PIN ... are easily cracked, because users picked passwords too easy to guess. 
The top worst password for the year 2013 was "**123456**", but there are other 
crazy sequences  like "**QWERTYUIOP**", "**0000**", animal names, birthdays ...

![no](img/cancel.png) **All these passwords, too easy to guess, must be avoided !** 
![no](img/cancel.png)

And, by the way, putting together two weak passwords, does not create a strong one ! 
"Independance1783" might be difficult to guess for a human being, but a "computer robot" 
will decipher it in the blink of an eye.

To **increase the robustness of your password**, in other words to increase its 
resistance against deciphering attacks, use as many characters as possible and mix 
their types (lowercase and uppercase letters, numbers, special characters).

There are simple tolls which allow you to keep and organize your passwords like 
KeePassX ([https://www.keepassx.org/](https://www.keepassx.org/)).

![noob](img/noob-modarp-circle-icon.png)  
**The stronger my password is, the more difficult to memorize it. Isn't it ?**  
You can use a **"pass-phrase**. Here we are talking about a long sentence, 
difficult to uncover, but easy to remember: few words put together produce a 
meaningless string, but which has a well defined sense for youself.  
For example, the sentance "grandma loves French pickles in her soup", can 
easily becomes a robust password: "GrandmaLovesFrenchPicklesInHerSoup" ... 
especially if one replaces few vowels by numbers ("i" by "1", "e" by "3" 
and o by "0"):  
"**GrandmaL0v3sFr3nchP1ckl3sInH3rS0up**".

## Protect your data

### Limit the access rights of others on your data

If you use Debian in "multi-user" mode, the data of the other users are readable 
by you, and yours too, by necessity. You may want to restrict the access rights 
to some of your data for the other users. The graphical procedure is easy (no 
need to open a terminal): right-click on he folder > properties > "Permissions". 
hereafter an example with the "Documents" folder.

![properties menu](img/droits-folder.png)

Select the "None" option for the "Others" access rights:

![no access for others](img/droits-folder-aucun.png)

A window will ask you if you want to apply these modifications to all the files 
and folders embedded within the concerned folder, and we advise you to accept, 
in order to protect the full set of data included inside this folder.

### Backup your data

You certainly ran into these warning messages talking about backing up your data ...  
and this is not for nothing! Please refer to the chapter 9 and get into 
**the habit of saving your data on an external medium and on a regular basis.**

## Antivirus software

Admittedly, the GNU/Linus systems are much less sensitive to virus attacks, 
but it is possible to find a virus on a GNU/Linux machine.

![noob](img/noob-modarp-circle-icon.png)  
For the time being, updates are the only efficient protections against potential 
viruses, and adding an antivirus software on your system does not improve its 
security. Viruses embedded within documents originating from proprietary systems 
do not target the GNU/Linux environments, and as such, are totally harmless for 
your data.

However, if you want to monitor and control your data, **ClamAv** is the reference 
antivirus software. Note that it does not run continuously in the background, and 
the user should explicitely request a *folder scan* to verify its contents.

More information on the DebianHelp site 
[http://www.debianhelp.co.uk/clamav.htm](http://www.debianhelp.co.uk/clamav.htm)

## Parental control

Yes, one can find everything on Internet: the best and the worst, and often 
inappropriate images and contents for our children. In order to let them enjoy 
safely the digital world, you can use different parental control systems.

**However, keep in mind that YOU are the best parental control !** ![](img/chevalier.gif)

### From your ISP

The Internet Service Providers usually propose different parental control software. 
This method allows you to control all the devices on your home network, but does not 
exempt you to activate the parental control on your Web Browser.

More information on the site 
[https://www.internetmatters.org/parental-controls/interactive-guide/](https://www.internetmatters.org/parental-controls/interactive-guide/)

### From your computer

The settings of the parental control is reserved to the experienced users, 
because the procedure is rather complex ... and is not 100% effective. In addition, 
the list of "prohibited" sites must be updated on a regular basis. The 
**Fox Web Security** addon module can be effectively installed in the Firefox 
navigator extensions 
([https://addons.mozilla.org/en-US/firefox/addon/fox-web-security](https://addons.mozilla.org/en-US/firefox/addon/fox-web-security)) 
in order to filter inappropriate "adult content".

An alternative solution is to use a search engine whis is going to filter the 
proposed results, like Qwant Junior: 
[https://www.qwantjunior.com](https://www.qwantjunior.com)

## Privacy on Internet

It is difficult to remain completely anonymous on Internet. Unless you are a 
well equipped experienced user, you will always leave a trail behind you.

The most beautiful fingerprint you leave, on a daily basis, is your IP address. 
In fact, each device connected to Internet must have an IP address, which allows 
to know not only your ISP, but also your precise geographical location ... A 
small test? visit this page to know your public IP address: 
[http://whatismyipaddress.com/](http://whatismyipaddress.com/)

In addition, even if you are not a great pastry chef, you are giving away a lot 
of "cookies" to all the sites you are visiting. Cookies are connection witnesses: 
they keep, for a given site, information like your preferences, your identifier, 
your password, your chosen language, the content of your digital shopping cart, 
etc. not only on your computer but also on the servers in the cloud. Thus, when 
you start a search request on a site, it is very capable to register this information.

Advertising companies come join the party, and automatically create your profile by 
looking at your browsing history.

Don't be alarmed, Numerous advices and software tools are at your disposal to 
become easily discreet.

### Social networks

Remember that the social networks, in their vast majority, are  not there to help you, 
quite the contrary: they are big advertising agencies which collect everything 
they find about you. Then, these personal information are resold to advertisers, 
in order for them to better target your "needs".  
Intelligence on the social networks does not stop there: they continue to trace you 
on plenty of sites by using - for example - the "like" or "G+" buttons.

Furthermore, the latest scandals unveil the fact that personal data can be collected 
by several governmental intelligence agencies, even if you are not doing anything 
suspicious.

![noob](img/noob-modarp-circle-icon.png)  
If you cannot resist to the temptation of opening an account on a social network 
(and I fully understand you: virtual encounters are very cool), you can test the 
**Diaspora** or the **Mastodon** journey and discover a decentralized network, 
which is **respectful of your private life and data**; Diaspora and Mastodon are 
designed on the "node network principle", which means that several connecting points 
are available. here is one to start with: 
[https://www.joindiaspora.com/](https://www.joindiaspora.com/) and 
[https://mastodon.social/about](https://mastodon.social/about).

As far as the other "less social" networks are concerned, find hereafter few 
simple advices:

- Use a pseudo instead of your name and forename.
- Limit your network to the persons you know in the real life.
- Modify your privacy settings in order to be seen by your "friends" only.
- Don't enter any personal information: address, phone number ...
- Avoid uploading pictures of yourself or your relatives, let alone tagging 
them (with the name of the persons on the photo).

## Private navigation within your browser

If you want to surf the Web without leaving traces on your computer, the latest 
versions of the Internet navigators include a "**private browsing**" feature. 
This mode functions very simply: once launched, the navigator does not keep the 
history of the visited sites, nor the cookies distributed by these sites, nor 
the passwords entered during this "private" session.  
However, the visited sites keep track of your IP address: you don't navigate in 
an "anonymous" way.

For Firefox, click on the menu (the 3 band top left button) then select the 
"New Private Window" option.

![Firefox: private navigation window](img/navigation_privee.jpg)

Note: it is very possible that some Internet sites do not function well if some 
cookies are disable. Thus, it is best not to use this mode all the time.

## Private navigation on Internet

**Warning: private does not mean anonymous!**

First avoid using non-free navigators, whose source codes cannot be analyzed by 
the first expert that comes along. The non-free navigators (Internet Explorer, 
Safari, Chrome ...) can potentially spy on your navigation without your knowledge.

The main free navigator you can trust is Firefox.

**The private navigation** allows the removal of all navigation traces on your 
computer and can add few more protections against the tracking. It cannot 
guarantee your anonymity when facing some advanced tracking technology like 
the fingerprinting 
([https://wikipedia.org/wiki/Canvas_fingerprinting](https://wikipedia.org/wiki/Canvas_fingerprinting)).

### Search engines

Stop doing like everyone else, even if supposedly "you have nothing to hide".

- **Do not make all your search requests on Google or Bing** ... even if they 
function very well, you might not want these companies to keep for tens of year 
your web search on "solution against hemorrhoids", or even to sell this 
information to drug companies, your mutual insurance company ...
- **Use search engines respecting your privacy** like Startpage 
([https://startpage.com](https://startpage.com)) (using the power of Google,
but hiding your identity to it), DuckDuckGo 
([https://duckduckgo.com/](https://duckduckgo.com/)) or QwantLite 
([https://lite.qwant.com/](https://lite.qwant.com/)).
- Enhance your free navigators with extensions fighting against the leakage 
of your data.

## Anonymous navigation on Internet

If you want to become anonymous and mask completely your IP address, the best 
idea is to use **TOR** 
([https://www.torproject.org/index.html](https://www.torproject.org/index.html)). 
To achieve this, we are going to desciobe two methods: installation of the 
**Tor-Browser** or the usage of the **Tails** *anonymous distribution*.

![noob](img/noob-modarp-circle-icon.png)  
**... What's this *Tor* stuff ??**  
**TOR** stands for **T**he **O**nion **R**outer, and is a software allowing us 
to become anonymous on the net by communicating with other TOR users. The 
principle is to define a random and indirect route on the network, between you 
and the exit node, which makes impossible the tracking of your IP address.  
In addition, the data circulating on the TOR network are encrypted, which makes 
even more difficult to identify the flows, although they are spied on.

This functionality must not prevent the common sense ... If you connect to an 
account, whatever it might be, with your usual pseudo and password, you will 
be quickly identified, even if you use TOR.

### Tor-Browser, an anonymous navigator

**Tor** offers its web browser under the form of an independent archive 
(no need for administrator rights):

- You begin by downloading the right version on the main site 
([https://www.torproject.org/projects/torbrowser.html.en](https://www.torproject.org/projects/torbrowser.html.en)) 
by selecting the language and the architecture:

![TorBrowser: the downloading page](img/torbrowser1.jpg)

- Once the archive is downloaded, uncompressed it wherever you want (but in a 
permanent folder) and open the newly created folder (tor-browser_en-US):

![TorBrowser: uncompressing the archive](img/torbrowser2.jpg)

![TorBrowser: opening the TorBrowser folder](img/torbrowser3.jpg)

- Double-click on the "Tor Browser Setup" icon:

![TorBrowser: Browser settings](img/torbrowser4.jpg)

- If your connecting site is behind a proxy, you must "Configure" the Tor 
connection. But in most of the cases, you can just click on the "Connect" 
button. The Tor service is started and look for relay nodes:

![TorBrowser: Tor connexion settings](img/torbrowser5.jpg)

![TorBrowser: connecting to the Tor network](img/torbrowser6.jpg)

- You can enjoy now the anonymous Internet navigation.

![TorBrowser: default interface](img/torbrowser7.jpg)

- If you want to have a quick access to the Tor Browser, drag-and-drop its icon 
on the launcher panel, or create on link on your desktop.

Please note that using Tor might slow down your Internet navigation.

### Tails: the anonymous distribution

**Tails** is a GNU/Linux distribution based on Debian. It allows you to be 
**totally anonymous** on the net. It is installed on a DVD or a USB key, 
and like with the other Debian Live (autonomous) images, nothing is saved 
on your DVD or USB key between two working sessions, and, in addition, you 
enjoy an anonymous navigation on Internet.

On the main site, you can read ...

![tails banner](img/tails_banner.png)\

Tails is a live (autonomous) operating system that you can start on almost 
any computer from a DVD, USB stick, or SD card. It aims at preserving your 
privacy and anonymity, and helps you to: 

- **use the Internet anonymously** and **circumvent censorship**; 
all connections to the Internet are forced to go through the Tor network;
- **leave no trace** on the computer you are using unless you ask it explicitly;
- **use state-of-the-art cryptographic tools** to encrypt your files, 
emails and instant messaging. 

For more information, you can visit the Tails download page 
[https://tails.boum.org/install/download/index.en.html](https://tails.boum.org/install/download/index.en.html).

![noob](img/noob-modarp-circle-icon.png)  
Note that the TOR navigation is often slowed down, and that some site or 
functionalities will not be reachable, due to their embedded scripts or 
their "privacy" policies ...  
Is it the right time to sort out through all your bookmarks ?

!["Plant onions!" by Péhä (CC-BY)](img/plantonion-by_peha.png)

### Real anonymity??

No! You must be aware that internet browsing works thanks to a **physical network 
of connected machines**. A person with physical access to the different 
"connection points" will be able to observe the traffic and capture informations 
immediately, or to store this information for a further consultation.  
In addition, computer monitoring is not the only way to identify you and your 
navigation: video surveillance of public or private places, labeling of machines 
"*for your safety*" are only examples of all the possibilities of the authorities 
and companies to access your valuable data.  
It is up to everyone to define his "private life" and not spread it on the web ...
Or to move on to more political than technical considerations for the Privacy
Protection ![wink](img/wink.png).

![Anonymous](img/anonymous.png)

