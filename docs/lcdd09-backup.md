# ![backup](img/anx.png) Back up your data

No matter where your expertize level is, or the current state of your hardware, 
nobody is immune to a bad cockpit error, a technical failure, a heavy thunderstorm, 
a cup of tea/coffee spilt on the keyboard, or the cat messing around with the central unit ...

The hard dish drive capacities are increasing every day and we are tempted to store 
on them more and more data (family pictures, videos, private copies of movies, etc). 
The risk of losing a large amount of data also increase at the same time, and that's 
why we advise you to execute regular backups of your personal data, as well as your 
passwords and email messages.

This section will endeavor to provide you with simple keys to avoid the lost of your 
favorite files during a hardware or software failure.

![noob](img/noob-modarp-circle-icon.png)  
The "cloud" is fashionable these days. Several on-line services are at your disposal 
to save your data on an external server...  
First of all, you are not immune to a server failure, and secondly, you have no real 
control on what your data are going to be used for. The "cloud" is actually the 
computer of someone else, as user Bibi told us recently.

I strongly advise you, in case of doubts (all services are not subsidiaries of the 
NSA ...) to backup your data locally, that is on your own physical medium which 
you fully control.

## Choosing the medium

![bkp](img/backup-support.png)  
Backups used to be made on floppy disks, then on CDs and then on DVDs. Even if you 
can still use this kind of support, the technology now gives you access to larger 
capacities at little cost.

Depending on the amount of the data to backup, you can find external disk from 1GB 
(USB key type) to 2TB (2000GB), being powered directly over the USB cable, or by 
an external power supply. The prices range roughly from 5 to 200 US$, depending 
the capacity.

Of course, if the size of your data is below 700MB, you can use a CD-RW 
(re-writable CD-Rom) for your backups.

## Graphical mode applications

![archive](img/archive.png)  
Debian hosts in its repositories several utilities 
([https://www.debian.org/doc/manuals/debian-reference/ch10.html#_copy_and_synchronization_tools](https://www.debian.org/doc/manuals/debian-reference/ch10.html#_copy_and_synchronization_tools)) 
in the "copy and synchronization" section, each of them offers a graphical 
interface or "client". Here we present one of the simplest backup tool: Deja-Dup.

**Deja Dup** is more than enough to backup your personal data, but if you 
want to execute a "full system backup" (including your application and the whole 
installation) you must use more complex software, as described in the Debian 
documentation 
([https://wiki.debian.org/BackupAndRecovery](https://wiki.debian.org/BackupAndRecovery)).

### Backing Up with Deja-Dup

![deja](img/deja-dup.png)  
**Deja Dup** ([https://wiki.gnome.org/Apps/DejaDup](https://wiki.gnome.org/Apps/DejaDup)) 
is a "simplified backup tool", which is a graphical interface to the **Duplicity** 
software ([http://duplicity.nongnu.org/](http://duplicity.nongnu.org/)). It allows 
building secured backups of your data within a local folder, an external disk, a local 
network, a remote network or somewhere in the "cloud".

It also allows the **complete encryption** and **password protection** of your backup files.

**Deja-Dup** offers a **clear interface** which does not require any computer knowledge.

#### Installing Deja-Dup

Deja-Dup can be found in the Debian repositories.To install it using a terminal in 
administrator mode (chap.3.8.3) :

    apt-get update && apt-get install deja-dup

Or using the graphical interfaces (chap.8.3), look for "deja-dup".

#### Launching and configuring Deja-Dup

**Deja-Dup** will be accessible from the application menu > "Utilities" > "Backups". 
During the first launch, the Deja-Dup settings let you define how to automatically 
execute the future backups, where to save, what to save and with which frequency.

![Deja-Dup: selection of the folders to save](img/dejadup_config1.png)

![Deja-Dup:selection of the folder to ignore](img/dejadup_config2.png)

![Deja-Dup: selection of the storage location](img/dejadup_config3.png)

![Deja-Dup: scheduling the backups](img/dejadup_config4.png)

#### Saving your data with Deja-Dup

Once the settings are done, **launch the first backup** by clicking on the 
"Back Up Now" button residing under the "Overview" tab.

![backup](img/dejadup_launch.png)

The very first backup execution will take some time, depending on the size of 
the data to be saved, but the following runs should be a lot faster since they 
will save only the modified files: this is the incremental backup.

An additional feature is the optional password setting during the configuration, 
which allows the encryption of the full set of backups:

![Deja-Dup: protecting your backup](img/dejadup_backup2.png)

![Deja-Dup: launching the backup](img/working_backup3.png)

#### Restoring your data with Deja-Dup

To **restore a backup** on a newly installed system, for example, simply install 
**Deja-Dup** on it, launch it and select "Restore" in the main window.

![restore launcher](img/dejadup_restore.png)

A series of simple windows will help you to find and restore your data:

- Define the restore from which location:

![](img/dejadup-bkp1.png)

- Define the restore from which creation time (when several backups are available):

![](img/dejadup-bkp2.png)

- Define the restore destination:

![](img/dejadup-bkp3.png)

- confirm the restore parameters:

![](img/dejadup-bkp4.png)

- If the backup has been encrypted, the password will be required, then the restore 
process will begin. An information window is displayed at the end of the process.

![Deja-Dup: restoring data](img/dejadup-runningbkp.png)

![Deja-Dup: restore process completed](img/dejadup-bkped.png)

## Manual method

![archive](img/archive.png)  
If you want to save just one folder or few of them, you can simply use the file 
system manager, or your archive manager (to reduce the storage space through compression).

The latter produces "real" archives: for subsequent consultation of these data, 
you need to uncompress the archive.  
From the file manager, select the folders to be saved, then right-click and take the 
"Compress..." (or "Archive") action.

Then you just have to move the archive on an external medium.

## Cloning the system

![clone](img/logo_clonezilla.png)  
This is the complete solution ensuring a total safety for your data: cloning the 
entire hard disk.

**Clonezilla Live** ([http://clonezilla.org/](http://clonezilla.org/)) is a Live CD 
based on the Debian GNU/Linux distribution, which includes the **Clonezilla** software. 
It allows the user to directly execute from its machine:

- **A Backup**: copy the entire disk copy , or one or several partitions, under 
the form of an image and save it on any kind of storage.
- **A Restore**: restore an image from its storage location (to the same disk, 
another disk, another machine, a USB key, a network, etc.).
- **A Copy**: direct copy from an original disk to another destination disk.

This Clonezilla version is able to connect to different servers: SSH server, 
Samba server, NFS server ...

As its name suggests it, it works like a Live CD (CD-ROM, DVD-ROM) but can also 
be executed from a USB key, an external disk, etc. 
(source [Wikipédia](https://en.wikipedia.org/wiki/Clonezilla)).

You can find an English tutorial on this page: 
[https://www.howtoforge.com/back-up-restore-hard-drives-and-partitions-with-clonezilla-live](https://www.howtoforge.com/back-up-restore-hard-drives-and-partitions-with-clonezilla-live).

