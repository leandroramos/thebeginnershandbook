# ![debian](img/debian-bouton.png) Debian? What's that?

![debian](img/debian-scratch.png) Distribution, free software, community, packages, source code ... but what is Debian in fact?

## The Debian GNU/Linux system

**Debian** is an operating system **libre** (as in free speech) and **gratis** (as in free beer). 
Debian allows your computer to function and offers you a a full set of **Free Software** for 
all the usual practices (surfing the Web, sending emails, playing multimedia files, 
doing office kind of tasks), and more ...![](img/smile.png)  
This collection of Libre Software comes to a large extend from the 
[GNU project](http://www.gnu.org/home.en.html), launched in 1983 by 
[Richard M. Stallman](http://en.wikipedia.org/wiki/Richard_Stallman). 
The [Linux kernel](http://en.wikipedia.org/wiki/Linux_kernel) developed by 
[Linus Torvalds](http://en.wikipedia.org/wiki/Linus_Torvalds) came to complete 
those software to make **GNU/Linux**.

![GNU & Tux, logos of the GNU projet and the Linux kernel by Péhä CC-BY-SA](img/avatar_tuxgnu_closed-modarp-220.png) 

The **Debian GNU/Linux** distribution was started by 
[Ian Murdock](https://en.wikipedia.org/wiki/Ian_Murdock) *(rip)* in August 1993. 
Everything started with a little, but solid,  group of *free software* hackers, 
which grew up to become a big and well organized community of developers and end users. 
**Debian** is now developed by a thousand of [volunteers](https://www.debian.org/devel/people) 
spread [around the world](https://www.debian.org/devel/developers.loc).

![in memory of Ian Murdock by Péhä CC-0](img/ian_murdock_20151230_by_peha.png)

So, **Debian** is a *complete set of free software*. A **free software** is defined by 
the [4 freedoms](https://www.gnu.org/philosophy/free-sw.en.html):  it gives the end users 
the freedom in **using**, **studying**, **sharing** and **modifying** that software, 
without breaking the law. To achieve this, it is necessary for the developer to distribute 
the source code and authorize the end-user to exercise its rights granted by a **free license**.  
One of the major interests of the free software is that it allows competent people to 
audit the program code, to insure notably that **it does only** what it is supposed to do. 
So it is additional barrier **to protect your privacy** ![](img/cool.png).

Debian implements this principle in its 
[Social Contract](https://www.debian.org/social_contract.en.html), 
and particularly in the 
[Free Software Guidelines](https://www.debian.org/social_contract.en.html#guidelines) according to Debian.  
This contract states that the Debian project will contain only free Software. 
Thus, during the installation of a *Debian* distribution, **neither non-free 
drivers will be install by default**. 
However the contract recognizes that some users might need "non-free" components to run their 
systems, like some peripheral drivers, or some applications decoding some music or video files, 
for example. That's why the distributed software is separated in 3 sections:

- **main** for the free software packages available by default,  
- **contrib** for the packages respecting the free software guidelines by themselves, 
but  are depending on non-free software, which do not comply with these guidelines, 
- **non-free** for packages which do not comply with the free software guidelines.

![logo Debian texte](img/logo_debian_txt_big.png)

Debian id developed very thoroughly. **Every new stable version** is 
carefully tested by users before it is released. And this release happens *when it is ready*. 
Hence **few maintenance work** is required once the system is installed and 
facing problems is very rare.

Like numerous other free distributions, Debian is not very sensitive to 
malware (like viruses Trojan horses, spyware...) and for several reasons:

- This large variety of software is available from **repositories** hosted on 
**servers controlled by the project**. Therefore, it is not necessary to search 
programs to be installed on dubious sites which distribute virus and 
unwanted programs in addition to the one you were looking for.
- The *administrator* and the *user* rights are clearly separated, which helps 
a lot in limiting the damages: In case of a viral infection, only the user's 
documents are affected. This clear separation of the rights limits also the risks 
of error made between the keyboard and the chair. More details on the rights in chapter 3.7.

![](img/chevalier.gif) **The back-up** of you data on a regular basis remains the best insurance 
to protect them against potential viruses or technical issues, 
but also against your own mistakes (chap.9).

## Where to find help

Do you need help? The first reflex, if you can, is to consult the documentation. 
Next comes the various user's forums, and then a GNU/Linux Group (LUG), if you are 
lucky enough to be located nearby. There are also several events dedicated to 
the free software in various associations: you will be able to define 
appointments not far from your home by consulting agendas of the Libre software 
[https://en.wikipedia.org/wiki/List_of_free-software_events](https://en.wikipedia.org/wiki/List_of_free-software_events).

- The **documentation embedded in the system** itself: in general, the installed applications 
include a manual available from the command line (chap.3.8) by typing 'man application_name' 
and/or from the graphical menu with the "Help" button of the application.
- The **on-line documentation**:  when you use a GNU/Linux distribution like Debian, 
you can access a detailed on-line documentation, with a list of the functionalities 
of the embedded applications. Debian provides you an official documentation: [https://wiki.debian.org](https://wiki.debian.org).
- Self-help and **support forums**: the free software community is divided 
into a host of forums, sites and blogs of information. To find your way in this 
abundance of communication is sometimes tricky, and you should rather prefer 
the sites dedicated to your own environment or distribution. 
Concerning Debian 2 main self-help forums are available to support you: 
Debian User forum ([http://forums.debian.net/](http://forums.debian.net/)) and 
the Debian Help ([forum http://www.debianhelp.org/](forum http://www.debianhelp.org/)). 
You could get some extra information on the Debian Official Support page: [https://www.debian.org/support](https://www.debian.org/support)
- **Associations and LUGs**: if you are lucky, you are living not too far from a 
Linux users group or an association where members meet on a regular basis. 
In this case don't hesitate to pay them a visit for a little chat 
([http://www.tldp.org/HOWTO/User-Group-HOWTO-3.html](http://www.tldp.org/HOWTO/User-Group-HOWTO-3.html))

### About forums, geeks, and the terminal

The self-help and support GNU/Linux community mainly consists of **passionate volunteers** 
who share their knowledge with big pleasure. They are also very technical and are friendly 
called the geeks (usually wearing a beard) with several years of computer practice behind them. 
This experience leads them to master the **terminal**, which is the most efficient tool to manage 
a GNU/Linux system: therefore, the very first answers found on the forums will naturally be given 
in the form of a set of command line operations. Don't be scared: in most of the cases a 
graphical solution (using the mouse within a window) exists. 
Ask kindly and you will get an explanation.

**To be able to ask a question on a self-help and support forum** you should usually 
register first. You need a valid email address  to register with, and receive a 
confirmation request message, as well as your answers notifications once registered.

**Before you ask a question**, please remember to look first into the Questions/Answers 
already solved: most of the forum include a search function by keyword, which will 
help you find out if your problem is already described in there and has a documented solution.

Don't forget that a forum is usually maintained by **volunteers**, not to be confused 
with a post-sales customer service organization ![](img/wink.png).

## Few links before moving on

- The Free Software Foundation: 
[https://www.fsf.org/?set_language=en](https://www.fsf.org/?set_language=en)
- About Debian: 
[https://www.debian.org/intro/about.en.html](https://www.debian.org/intro/about.en.html)
- Introduction to Debian: 
[https://www.debian.org/intro/index.en.html](https://www.debian.org/intro/index.en.html)
- The official Debian Wiki: 
[https://wiki.debian.org/DebianIntroduction](https://wiki.debian.org/DebianIntroduction)
- the developer's corner: [https://www.debian.org/devel/](https://www.debian.org/devel/)
- the historic details: 
[https://www.debian.org/doc/manuals/project-history/](https://www.debian.org/doc/manuals/project-history/)
- The joy of coding [https://twitter.com/joyofcoding](https://twitter.com/joyofcoding)

