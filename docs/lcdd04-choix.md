# ![choix](img/choix.png) Pick your Debian

Debian is an operating system offered in several versions, and you will need to 
make a choice  depending on your tastes, your needs and your hardware.

### Choosing a Debian derivative?

There are a lot of Debian derivatives out there, more or less stable and/or 
supported. This manual and the indications in there can be used for all the 
Debian Stable "Stretch" derivatives.

![noob](img/noob-modarp-circle-icon.png)  
The GNU/Linux distributions are not all supported by a community the way 
Debian is. Sometime, a single person handle the entire distribution. The 
different distributions do not provide the same level of documentation and 
support. They also do not offer the same durability. Choosing a "mother" 
distribution like Debian insures you to receive stable and regular updates.

If you want to test other distributions based on Debian, we suggest you consult 
the Debian derivatives official page 
([https://wiki.debian.org/Derivatives/CensusFull](https://wiki.debian.org/Derivatives/CensusFull)).

If you want to consult a more comprehensive list, visit 
[Distrowatch.com](https://distrowatch.com/?language=EN) which enumerates almost 
all the available active distributions.

### Alternatives to Debian

Because no, there is not only "Debian" in life ![](img/wink.png)  
Other projects develop systems for beginners in the GNU/Linux world. If you want 
to try another system, let's take a look at the GNU/Linux distributions dedicated 
chapter in the Software Directory (chap.12).

### You want to try the Debian journey

Let's go ! The rest of this manual will help you to:

- **choose the processor architecture** (chap.4.1),
- **choose your main interface** (chap.4.2),
- **launch the installation** (chap.5).

![logo debian](img/debian-90.png)

## Choosing the architecture

**The processor**, which is the compute and control unit of your computer, functions 
under a specific type of architecture, I.E. it handles data according to its own 
type (32 or 64 bits, PowerPC ...).

We encourage you to use an ISO image of type "**netinst**" (chap.5.2.1.1), which 
gives you access to the most up-to-date software versions by downloading directly 
the applications during the installation process (you need an active and stable 
Internet connexion during the install). This version will let you install the 
desktop of your choice.  
You can also use an ISO image already including a ready-to-use environment 
(Gnome, KDE, Xfce ...).

But you must choose an ISO corresponding to your processor architecture. In other 
words, decide between a 32 bits or a 64 bits version, or again between an i386 or 
an amd64 version.  
Note that the "32 bits" version will run on a "64 bits" computer, but not vice-versa.

### To make it easy

- If your computer has a sticker indicating "coreDuo", "core2duo", or was built 
after the year 2005, you can take an "amd64" ISO.
- If your computer proudly displays the sign "Pentium IV M" or was born before 
2003, take an "i386" version.
- In case of doubts, you can take the 32 bits "i386" version, it functions everywhere.
- In case of a huge doubt, ask the question on a forum (chap.1.2)

### Checking the architecture

**Using a GNU/Linux distribution** in an autonomous "live" session (chap.5.3.1), 
open a terminal, and in order to find out the 32 or 64 bits compatibility, type the 
following command:

    lscpu | grep -i "mode(s)"

which returns the explicit result, here an example when using an 'amd64' processor:

    CPU op-mode(s): 32-bit, 64-bit

**Using a Windows**® system, your processor model is displayed in the "General" tab 
of the menu Start Button > Configuration Panel > System

## Choosing your GNU/Linux desktop

Debian let you adopt one or several desktop environment directly from the installer 
interface: [Gnome](#gnome), [KDE](#kde), [Mate](#mate), [Cinnamon](#cinnamon), 
[Xfce](#xfce) and [LXDE](#lxdeopenbox). We propose you to discover also [LXQt](#lxqt).

![noob](img/noob-modarp-circle-icon.png)  
**... But why several Gnu/Linux "desktop" ?**  
**The computer is only a tool**, and even if we ask you sometime to learn a couple 
of tricks to make an effective use of it, it is still **your tool**, and as such 
it must be highly adaptable to your taste, your working habit, and to your own 
way to have fun.

With this perspective, each desktop has its own organization, its setting method, 
its various functionalities. And it is not a superfluous diversity, designed by 
the brain mazes of some bearded geeks (although...), but rather a strength of 
the **free software**: knowing how to **propose rather than impose!** ![](img/cool.png)

Hereafter the description of the main characteristics of this various environments, 
to help you decide which one to use. And then you will be able to add more of them, 
according to your needs and preferences.  
The two main desktops are Gnome and KDE, but they are also the more "resource-hungry" 
solutions, so don't hesitate to test other environments.

**Integrated applications**

Each desktop environment is designed to offer a minimal functionality for the 
personal usage of your computer.  
So, within all the desktops presented here, you will find the Firefox Internet browser 
(chap.6.6), a system file manager, a text editor, the LibreOffice suite (chap.6.9), 
a multimedia player (except on MATE DE, but it's easy to add one) and an access to 
a software library in order to maintain, update or make changes in your installation.

### Gnome ![gnome](img/gnome-mini.png)

The Gnome-Shell interface is the default desktop for Debian and offers:

**The Activities Overview"**, which is a separate view designed to enable users to 
get an overview of their current activities and to enable effective focus switching. 
It can be accessed via the activities button at the left of the top bar or by 
pressing the ''⊞ Win'' key of the keyboard. This view includes:

- A series of application launchers (that you can easily add or remove using the 
drag and drop method, or by using a right-click on the "dock").
- All the open applications.
- The capability to dispatch the applications to several virtual desktops (visualized 
on the right side pane).
- A seach engine for applications, files or even contacts.

![Gnome: desktop presentation](img/deb9-gnome-bureau-virtuel.png)

**The Time manager**, at the center of the top bar, has an on click calendar, 
linked to Evolution (the mail client), the personal information manager handling 
emails, calendar and contacts.

![Gnome : le gestionnaire de temps](img/deb9-gnome-time.png)

**The System manager** at the right of the top bar, including:

- The settings of the local network parameters
- The settings of the sessions parameters
- Access to the system parameters settings

![Gnome: the System manager](img/deb9-gnome-system.png)

**Integrated comprehensive help**

Gnome includes a general detailed help mechanism, that you can access via the dock, 
by default, and this should reassure all the beginners, and make them a lot more 
comfortable:

![Gnome: integrated comprehensive help](img/deb9-gnome-help.png)

**Main applications:**

- Internet communications: Evolution 
([https://wiki.gnome.org/Apps/Evolution](https://wiki.gnome.org/Apps/Evolution)) 
and Polari ([https://wiki.gnome.org/Apps/Polari](https://wiki.gnome.org/Apps/Polari))
- Video player: Totem (chap.6.7)
- Audio player: Rhythmbox (chap.6.8)
- System file manager: Nautilus 
([https://wiki.gnome.org/Apps/Nautilus](https://wiki.gnome.org/Apps/Nautilus))

**Minimum RAM needed to start up:** 450 Mo  
**Minimum RAM needed to surf the web:** 530 Mo  
**Minimum user level:** extreme beginner / beginner

Gnome project home page: [https://www.gnome.org/](https://www.gnome.org/)

More details in the dedicated section of this manual (chap.7.1.1).

### KDE ![kde](img/kde-mini.png)

**KDE** is a project delivering a graphical environment (called "Plasma") and 
a full set of applications. KDE is highly configurable, both in terms of desktop 
environment and supported applications. KDE has so many capabilities that some 
people even find difficult to deal with it.  
Note that you can use KDE without any particular configuration tuning. It can 
be fully functional with its "out-of-the-box" settings.

![The KDE Plasma desktop on Debian](img/deb9-kde-desktop-shot.png)

The KDE environment is visually close to Windows®, and offers:

- **KDE Application launcher**: the menu giving you access to the entire system.
- **Graphical components**: the modules you can place anywhere on your desktop 
to enjoy additional functionalities.
- **Activity manager**: a banner which integrates your pending tasks, your 
appointments... another way to help with your daily work.
- **Plasma**: KDE is delivered with a pack of applications integrated in the 
"plasma" desktop, and providing a fluid and consistent graphical experience.

More than any other GNU/Linux desktop, you can transform KDE according to your 
needs and desires.  
KDE is a very complete environment and has a configuration center as well as a 
built-in help center.  
To launch the control center, navigate to the main menu > Applications > 
Configuration > System Settings.

![KDE System Settings on Debian](img/deb9-kde-settings.png)

If you can not find an application, you can enter its name or function in the 
search box of the main menu (here with the help center).

![KDE Help Center launcher](img/deb9-kde-help-search.png)

![KDE Help Center](img/deb9-kde-help-center.png)

**Main applications:**

- Internet Navigator: Konqueror ([https://konqueror.org/](https://konqueror.org/))
- Internet communications: Kmail 
([https://userbase.kde.org/KMail/fr](https://userbase.kde.org/KMail/fr)), Kopete 
([https://userbase.kde.org/Kopete/fr](https://userbase.kde.org/Kopete/fr))
- Video player: Dragon Player 
([https://www.kde.org/applications/multimedia/dragonplayer/](https://www.kde.org/applications/multimedia/dragonplayer/))
- Audio player: Juk ([https://juk.kde.org/](https://juk.kde.org/))
- File system manager: Dolphin 
([https://www.kde.org/applications/system/dolphin/](https://www.kde.org/applications/system/dolphin/))

**Minimum RAM needed to start up:** 480 Mo  
**Minimum RAM needed to surf the web:** 570 Mo  
**Minimum user level:** beginner / experienced

KDE project home page: [https://www.kde.org/](https://www.kde.org/)  
An introduction to KDE: 
[https://userbase.kde.org/An_introduction_to_KDE](https://userbase.kde.org/An_introduction_to_KDE)

More detailed information in the dedicated section of this manual (chap.7.1.2).

### Mate ![mate](img/mate-mini.png)

**MATE** is a fork of GNOME 2. This means that this desktop environment was 
started from a copy of  GNOME 2 and then modified independently.

This is the perfect environment for people who do not want to change all the 
work habits they acquired when using GNOME 2. This is also a good system for 
the computer aided music and the resource-intensive applications going with.

Mate aims to be lighter than GNOME 3, and as such, is  more suitable for aging 
or resource limited computers. It's clearly is a good compromise between GNOME 
3 and Xfce 4.

![MATE desktop on Debian](img/mate_desktop.png)

Mate offers a "traditional" interface with its two task bars:

- The top panel includes, on the left, the main applications, the main folders 
and the system setting menu.
- The right part of the top panel is reserved to the notification area and the 
quick settings (Audio volume, keyboard configuration, small agenda)
- The bottom panel includes the desktop button (to mask all the application 
windows), the list of the active windows, and the virtual desktops selector.

To configure Mate you open the control center ("system" menu > Control Center) 
and you have access to the various components of this interface:

![Control Center of the MATE desktop](img/mate_control.png)

- System file manager: Caja (fork of Nautilus)
- Minimum RAM needed to start up: 310 Mo
- Minimum RAM needed to surf the web: 390 Mo
- Minimum user level: extreme beginner / beginner

Mate project home page: [http://mate-desktop.com](http://mate-desktop.com)  
More details on the Mate Debian wiki: 
[https://wiki.debian.org/Mate](https://wiki.debian.org/Mate)

### Cinnamon ![cinnamon](img/cinnamon-mini.png)

**Cinnamon** is a desktop environment derived from the Gnome-Shell project. 
It moved away from this latter all-in-one interface, to adopt a more traditional 
interface (dash board along with a menu where Icons are sorted by categories).

Cinnamon is developed by the Linux Mint team 
([https://linuxmint.com/](https://linuxmint.com/)), and is one of the leading 
desktop environments of this specific GNU/Linux distribution.

Cinnamon uses the **Nemo** system file manager, a fork from Nautilus, which 
reinstates some functionalities removed by the latter: compact view, open in 
a terminal, open as root, capability to manually edit the absolute file path, etc.

![Cinnamon desktop on Debian 9](img/deb9-cinnamon-desktop.png)

Cinnamon offers a complete desktop with all the helpful (or not) applications for 
your daily usage: in the screen capture above, you can see the graphical 
applications, and under the Internet category, for example, you will find the 
Firefox navigator (chap.6.6), the email client Thunderbird (chap.6.4.1), the 
multi-protocol chat client Pidgin, the Bittorent Transmission client and a remote 
desktop access software.

The Cinnamon configuration is delegated to the system settings pannel which 
centralized all the system and user settings:

![Cinnamon: system settings panel](img/deb9-cinnamon-control-center.png)

**Main applications:**

- Internet communications: Thunderbird (chap.6.4.1) & Pidgin
- Video player: Totem (chap.6.7)
- Audio player: Rythmbox (chap.6.8)
- System file manager: Nemo

**Minimum RAM needed to start up:** 480 Mo  
**Minimum RAM needed to surf the web:** 560 Mo  
**Minimum user level:** extreme beginner / beginner

Cinnamon project home page: 
[https://github.com/linuxmint/Cinnamon](https://github.com/linuxmint/Cinnamon)  
More detailed information on the CinnamonTeam: 
[https://wiki.debian.org/CinnamonTeam](https://wiki.debian.org/CinnamonTeam)

### Xfce ![xfce](img/xfce-mini.png)

**Xfce** is a lightweight desktop environment for Unix type operating systems. 
Its goals are to be fast, little greedy in machine resources, but visually 
attractive and user friendly.
It is expandable, thanks to numerous available plug-ins, and embeds a volume 
control application (xfce4-mixer), its own window manager supporting 
transparency, shades … (xfwm4), an integrated archive manager (thunar-archive-plugin), 
and disk, battery network, processor and memory monitorings, as well as various 
themes and miscellaneous pug-ins.

This interface is clear and traditional: you won't be surprised during your first 
meeting with it.

![The default Xfce on Debian 9](img/deb9-xfce-desktop.png)

Xfce has the significant advantage to be fully modular, because it is released 
with different independent plug-ins. Xfce also allows the integration of 
applications coming from other environments, and is able to launch, during the 
system start-up, the Gnome and/or KDE services by default.

**It is a perfect environment for beginners**, offering a great stability, a 
complete graphical handling ("with the mouse only"), and evolution capabilities 
with no real limit.

Like the other previously seen environments, Xfce centralizes its configuration 
settings to ease the customization. Note that each elements can also be configured 
from its specific interface (like a right-click on the panel to add a new 
launcher, for example).

![The Xfce setting center on Debian 9](img/deb9-xfce-control-center.png)

**Main applications:**

- Internet communications: Thunderbird (chap.6.4.1)
- Video player: VLC (chap.6.7)
- Audio player: Quod Libet 
([https://quodlibet.readthedocs.io/en/latest/](https://quodlibet.readthedocs.io/en/latest/))
- System file manager: Thunar (chap.3.6)

**Minimum RAM needed to start up:** 250 Mo  
**Minimum RAM needed to surf the web:** 330 Mo  
**Minimum user level:** extreme beginner / beginner

Xfce project home page: [http://www.xfce.org/](http://www.xfce.org/)  
More detailed information on the Xfce Debian wiki: 
[https://wiki.debian.org/Xfce](https://wiki.debian.org/Xfce).

### LXDE/Openbox ![lxde](img/lxde-mini.png)

**LXDE** is a free desktop environment for Unix like systems, and other systems 
which are compliant to the POSIX standard, like Linux or BSD (Berkeley Software 
Distribution). The LXDE name is the acronym for "Lightweight X11 Desktop Environment".  
And as its name implies, the goal of this project is to propose a fast and lightweight 
desktop environment.

As opposed to other desktop environments, the various components are not tightly 
linked together. Instead they are rather independents and each of them can be used 
without the others, with very few dependencies (packages used during the installation).

LXDE uses by default Openbox 
([https://wiki.debian.org/Openbox](https://wiki.debian.org/Openbox)) as window manager:

![The LXDE desktop and PCManFM on Debian](img/deb9-lxde-desktop-shot.png)

The LXDE design model implies that the configuration of each element needs to go 
through an interface designed for this specific application. You will not find a 
"Control Center" akind to the Gnome one, but rather a suite of lightweight tools 
to customize your environment.

![LXDE Appearance and Session configuration panels on Debian](img/lxde_control1.png)

![Preference menu and Openbox configuration tool on LXDE](img/lxde_control2.png)

Note that the elements being configured, display their modifications on the fly 
which makes the customization very easy.

LXDE is notably light, making it an ideal solution for small hardware configurations 
and computers refurbishing, but it requires a little more time to learn and use its 
different elements.

Having said that, the LXDE/OpenBox couple will let you earn your first stripes on 
Debian as a "g33k".

**Main applications:**

- Video player: SMplayer ([http://smplayer.sourceforge.net/](http://smplayer.sourceforge.net/))
- Audio player: LXMusic ([http://wiki.lxde.org/fr/LXMusic](http://wiki.lxde.org/fr/LXMusic))
- System file manager: PCManFM ([https://wiki.lxde.org/fr/PCManFM](https://wiki.lxde.org/fr/PCManFM))

**Minimum RAM needed to start up:** 220 Mo  
**Minimum RAM needed to surf the web:** 320 Mo  
**Minimum user level:** beginner / experienced

LXDE project home page: [http://lxde.org](http://lxde.org)

More detailed information on the LXDE official wiki: [https://wiki.lxde.org](https://wiki.lxde.org)

### LXQt ![lxqt](img/lxqt-mini.png)

**LXQt** is a light desktop environment. It offers a classical but a modern and lightweight 
interface that will let you work without thinking about it. It comes with a series of 
specific tools to take advantage of your favorite data and applications. It is an ideal 
environment for old machines and low performance.

![LXQt desktop on Debian 9](img/deb9-lxqt-desktop.png)

Its lightness does not dispense with a centralized configuration tool allowing you 
to access the settings of the main functions of your system.  
Configuration goes through the control center: direction the main menu > 
Preferences > LXQt Settings> LXQt configuration center:

![LXQt configuration](img/deb9-lxqt-config.png)

LXQt is not yet available from the standard Debian 9 installation interface, but you 
can easily install it from your software manager (chap.8.3)

**Main applications:**

- Video player: SMplayer
- Audio player: Audacious
- System file manager : PCManFM

**Minimum RAM needed to start up:** 220 Mo  
**Minimum RAM needed to surf the web:** 310 Mo  
**Minimum user level:** beginner / experienced

LXQt homepage: [http://lxqt.org/](http://lxqt.org/)

