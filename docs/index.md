![logo cahiers](img/logo_cahiers_big.png)

![debian text mini](img/logo_debian_txt_mini.png) *without headaches*

![deb9](img/debian_stretch.png)

[https://lescahiersdudebutant.fr/index-en.html](https://lescahiersdudebutant.fr/index-en.html)

**-- About this manual --**

**"The beginner's handbook"** is a simplified manual to install and take-over the 
Debian system.

You will find in the following pages **the answers to your first questions** 
concerning the **Debian GNU/Linux** system ![](img/gnulinux-mini.png), its history, 
how to obtain it, to install it, to take-over it, to configure and administrate it.

You will be able to **go further** and obtain information concerning the privacy 
protection, the backing up of your data, and the various actors of the Free 
Software world.

Usually, the manuals begin by teaching you the theoretical basis and the usage of 
the terminal. This manual takes the very side of the "graphical environment": it 
is designed to let you start quickly with Debian, screen powered on, fingers on 
the keyboard and the mouse nearby ![](img/big_smile.png).

**-- The mission of this manual is not to be comprehensive. --**

A lot of external links are available in this manual. Don't hesitate to click on 
them in order to read more detailed information.

For a more detailed documentation, please visit the official Debian Wiki: 
[https://wiki.debian.org/FrontPage](https://wiki.debian.org/FrontPage)

If you need a complete Debian manual, please read the **Debian Administrator Handbook** 
from Raphaël Hertzog and Roland Mas 
[https://debian-handbook.info/browse/stable/](https://debian-handbook.info/browse/stable/).

**-- How to use this manual?--**

Iy depends on the version you are viewing :

- the [HTML version](https://dflinux.frama.io/thebeginnershandbook/) has an effective 
search field, do not hesitate to enter a word or two,
- the [PDF version](https://framagit.org/dflinux/thebeginnershandbook/raw/master/docs/the_beginners_handbook.pdf) 
includes a summary *more than* detailed and a table of figures at the end of the guide. 
This version is included in the 
[debian package](https://framagit.org/dflinux/thebeginnershandbook/tree/master/docs/paquet_debian/),
- the [HTML-archive version](https://framagit.org/dflinux/thebeginnershandbook/raw/master/docs/the_beginners_handbook.html.tar.gz) 
opens on a single page, which makes it easy to search from your browser using the shortcut [Ctrl]+'f'
- the [ePub version](https://framagit.org/dflinux/lescahiersdudebutant/raw/master/docs/les_cahiers_du_debutant.epub) 
takes advantage of the detailed table of contents and search functions of your ebook reader.

---

The Debian beginner's handbook: 
[https://lescahiersdudebutant.fr/index-en.html](https://lescahiersdudebutant.fr/index-en.html)

![cahiers minibanner](img/minibanner-cahiers.png)

